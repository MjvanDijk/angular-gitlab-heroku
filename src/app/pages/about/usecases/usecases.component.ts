import { Component, OnInit } from '@angular/core'
import { UseCase } from '../usecase.model'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.scss']
})
export class UsecasesComponent implements OnInit {
  readonly PLAIN_USER = 'Reguliere gebruiker'
  readonly ADMIN_USER = 'Administrator'
  readonly LOCATION_USER = 'locatieMedewerker'

  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: this.PLAIN_USER,
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd'
    },
    {
      id: 'UC-02',
      name: 'Afspraak maken',
      description: 'Hiermee kan een bezoeker een Afspraak maken',
      scenario: [
        'Kies met wie je en afsrpaak wil maken',
        'kiest de juise datum, tijd en klikt op afsrpaak maken',
        'Applicatie kijkt of datum en tijd besckikbaar zijn',
        'Indien gegevens correct zijn dan redirect de applicatie naar bevestegings scherm'
      ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'Afspraak is gemaakt'
    },
    {
      id: 'UC-03',
      name: 'Afsrpaak annuleren',
      description: 'Hiermee kan je afsrpaak annuleren',
      scenario: [
        'klik op afsrpaak annuleren',
        'Applicatie kijkt of afspraak bestaat',
        'Indien gegevens correct zijn dan redirect de applicatie naar bevestings scherm'
      ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'Afsrpaak is geanuleerd'
    },
    {
      id: 'UC-04',
      name: 'afspraak annuleren',
      description: 'Hiermee kan medewerker een afsrpaak annuleren',
      scenario: [
        'klikt in lijst van afspraken die ene die geanuleerd moet worden',
        'klikt op annuleren',
        'Indien gegevens correct zijn dan redirect de applicatie naar bevestegings scherm'
      ],
      actor: this.LOCATION_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'reservering geanuleerd'
    },
    {
      id: 'UC-05',
      name: 'Overszicht Afspraken',
      description: 'Hiermee kan medewerker alle afsrpaken zien op de locatie',
      scenario: [
        'klikt in menu op afspraken',
        'applicatie haalt alle afspraken naatr boven',
        'gaat naar scherm met alle afsrpaken'
      ],
      actor: this.LOCATION_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'afsrpaak pagina'
    },
    {
      id: 'UC-06',
      name: 'Overszicht Afspraken',
      description: 'Hiermee kan gebruiker zijn afsrpaken zien',
      scenario: [
        'klikt in menu op afspraken',
        'applicatie haalt alle afspraken naatr boven',
        'gaat naar scherm met alle afsrpaken'
      ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'afsrpaak pagina'
    }
  ]

  constructor() {}

  ngOnInit() {}
}
